import AerialVehicles.Drones.Hermes.Kochav;
import AerialVehicles.FlightPlanes.F15;
import Components.Camera;
import Components.Component;
import Components.MissileContainer;
import Components.Sensor;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Enum.*;
import Missions.BdaMission;
import Missions.IntelligenceMission;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        try {
            Coordinates destination = new Coordinates(1.1, 2.2);
            ArrayList<Component> components = new ArrayList<>();
            components.add(new Sensor(SensorType.ELINT));
            components.add(new Camera(CameraType.REGULAR));
            components.add(new MissileContainer(20, MissileType.PYTHON));

            Kochav kochav1 = new Kochav(0, FlightStatus.READY, destination, components );
            Kochav kochav3 = new Kochav(3000, FlightStatus.READY, destination, components);
            Kochav kochav2 = new Kochav(0, FlightStatus.NOT_READY, destination, components);

            AttackMission attackMission = new AttackMission(destination, "Muhamad", kochav1, "My house");
            IntelligenceMission intelligenceMission = new IntelligenceMission(destination, "Omer", kochav3, "Ha'arava");
            BdaMission bdaMission = new BdaMission(destination, "Shlomo", kochav2, "meow place");

            attackMission.begin();
            System.out.println("---------------");
            intelligenceMission.begin();
            System.out.println("---------------");
            bdaMission.begin();
            System.out.println("---------------");
            System.out.println(attackMission.finish());
            System.out.println("---------------");
            System.out.println(intelligenceMission.finish());
            System.out.println("---------------");
            System.out.println(bdaMission.finish());
            System.out.println("---------------");

            F15 f15 = new F15(0, FlightStatus.NOT_READY, destination);
            f15.addComponent(new MissileContainer(20, MissileType.PYTHON));
            f15.addComponent(new Sensor(SensorType.ELINT));
            BdaMission failure = new BdaMission(destination, "Meow", f15, "there");
        } catch (AerialVehicleNotCompatibleException e) {
            System.out.println(e.getMessage());
        }
    }
}
