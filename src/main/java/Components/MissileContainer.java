package Components;

import Enum.*;

public class MissileContainer implements Component {
    private int numOfMissiles;
    private MissileType missileType;

    public MissileContainer(int numOfMissiles, MissileType missileType) {
        this.numOfMissiles = numOfMissiles;
        this.missileType = missileType;
    }

    public int getNumOfMissiles() {
        return numOfMissiles;
    }

    public void setNumOfMissiles(int numOfMissiles) {
        this.numOfMissiles = numOfMissiles;
    }

    public MissileType getMissileType() {
        return missileType;
    }

    public void setMissileType(MissileType missileType) {
        this.missileType = missileType;
    }
}
