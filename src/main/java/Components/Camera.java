package Components;

import Enum.*;

public class Camera implements Component {
    private CameraType cameraType;

    public Camera(CameraType cameraType) {
        this.cameraType = cameraType;
    }

    public CameraType getCameraType() {
        return cameraType;
    }

    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }
}
