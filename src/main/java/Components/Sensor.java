package Components;

import Enum.*;

public class Sensor implements Component {
    private SensorType sensorType;

    public Sensor(SensorType sensor) {
        this.sensorType = sensor;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensor) {
        this.sensorType = sensor;
    }
}
