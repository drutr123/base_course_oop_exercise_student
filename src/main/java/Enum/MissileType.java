package Enum;

public enum MissileType {
    PYTHON("Python"),
    AMARAM("Amaram"),
    SPICE250("Spice250");

    String value;

    MissileType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
