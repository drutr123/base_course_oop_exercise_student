package Enum;

public enum CameraType {
    REGULAR("Regular"),
    THERMAL("Thermal"),
    NIGHT_VISION("NightVision");

    String value;

    CameraType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
