package Enum;

public enum FlightStatus {
    READY,
    NOT_READY,
    IN_AIR
}
