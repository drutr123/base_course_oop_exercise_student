package Enum;

public enum SensorType {
    INFRA_RED("InfraRed"),
    ELINT("Elint");

    String value;

    SensorType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
