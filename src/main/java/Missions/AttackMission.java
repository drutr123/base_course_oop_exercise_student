package Missions;

import AerialVehicles.AerialVehicle;
import Components.Component;
import Components.MissileContainer;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(Coordinates missionTarget, String pilotName, AerialVehicle aerialVehicle, String target) throws AerialVehicleNotCompatibleException {
        super(missionTarget, pilotName, aerialVehicle);
        this.target = target;

        if (!aerialVehicle.hasComponent(MissileContainer.class)) {
            throw new AerialVehicleNotCompatibleException("Vehicle does not have missiles");
        }
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public String executeMission() {
        MissileContainer missileContainer = (MissileContainer) super.getAerialVehicle().findComponent(MissileContainer.class);
        return super.getPilotName() + ": " + super.getAerialVehicle().getClass().getSimpleName() + " Attacking "
                + this.getTarget() + " with: " + missileContainer.getMissileType().getValue() + "X" + missileContainer.getNumOfMissiles();
    }
}
