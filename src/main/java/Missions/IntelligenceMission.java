package Missions;

import AerialVehicles.AerialVehicle;
import Components.Camera;
import Components.Sensor;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates missionTarget, String pilotName, AerialVehicle aerialVehicle, String region) throws AerialVehicleNotCompatibleException {
        super(missionTarget, pilotName, aerialVehicle);
        this.region = region;

        if (!aerialVehicle.hasComponent(Sensor.class)) {
            throw new AerialVehicleNotCompatibleException("Vehicle does not have sensor");
        }
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String executeMission() {
        return super.getPilotName() + ": " + super.getAerialVehicle().getClass().getSimpleName() + " Collecting Data in "
                + this.getRegion() + " with: sensor type: " + ((Sensor)super.getAerialVehicle().findComponent(Sensor.class)).getSensorType().getValue();
    }
}
