package Missions;

import AerialVehicles.AerialVehicle;
import Components.Camera;
import Components.MissileContainer;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates missionTarget, String pilotName, AerialVehicle aerialVehicle, String objective) throws AerialVehicleNotCompatibleException {
        super(missionTarget, pilotName, aerialVehicle);
        this.objective = objective;

        if (!aerialVehicle.hasComponent(Camera.class)) {
            throw new AerialVehicleNotCompatibleException("Vehicle does not have camera");
        }
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    @Override
    public String executeMission() {
        return super.getPilotName() + ": " + super.getAerialVehicle().getClass().getSimpleName() + " taking pictures of "
                + this.getObjective() + " with: " + ((Camera)super.getAerialVehicle().findComponent(Camera.class)).getCameraType().getValue();
    }
}
