package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {
    private Coordinates missionTarget;
    private String pilotName;
    private AerialVehicle aerialVehicle;

    public Mission(Coordinates missionTarget, String pilotName, AerialVehicle aerialVehicle) {
        this.missionTarget = missionTarget;
        this.pilotName = pilotName;
        this.aerialVehicle = aerialVehicle;
    }

    public Coordinates getMissionTarget() {
        return missionTarget;
    }

    public void setMissionTarget(Coordinates missionTarget) {
        this.missionTarget = missionTarget;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public AerialVehicle getAerialVehicle() {
        return aerialVehicle;
    }

    public void setAerialVehicle(AerialVehicle aerialVehicle) {
        this.aerialVehicle = aerialVehicle;
    }

    public void begin() {
        System.out.println("Beginning Mission!");
        this.getAerialVehicle().flyTo(this.getMissionTarget());
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        this.getAerialVehicle().land(this.getAerialVehicle().getBaseCoordinates());
    }

    public String finish() {
        this.getAerialVehicle().land(this.getAerialVehicle().getBaseCoordinates());
        System.out.println("Finish Mission!");
        return this.executeMission();
    }

    public abstract String executeMission();
}
