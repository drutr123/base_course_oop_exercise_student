package AerialVehicles.Drones;

import AerialVehicles.AerialVehicle;
import Components.Component;
import Entities.Coordinates;
import Enum.*;

import java.util.ArrayList;

public abstract class Drone extends AerialVehicle {
    public Drone(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Drone(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }

    public  String hoverOverLocation(Coordinates destination) {
        this.setFlightStatus(FlightStatus.IN_AIR);
        String output = "Hovering Over: " + destination.toString();
        System.out.println(output);

        return output;
    }
}
