package AerialVehicles.Drones.Hermes;

import Components.Camera;
import Components.Component;
import Components.Sensor;
import Enum.*;
import Entities.Coordinates;

import java.util.ArrayList;

public class Zik extends Hermes {
    public Zik(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Zik(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }
}