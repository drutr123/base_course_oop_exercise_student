package AerialVehicles.Drones.Hermes;

import AerialVehicles.Drones.Drone;
import Components.Component;
import Entities.Coordinates;
import Enum.*;

import java.util.ArrayList;

public abstract class Hermes extends Drone {
    private final int MAX_HOURS_FOR_REPAIR = 100;

    public Hermes(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Hermes(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }

    @Override
    public int getMaxHoursForRepair() {
        return this.MAX_HOURS_FOR_REPAIR;
    }
}
