package AerialVehicles.Drones.Hermes;

import Components.Camera;
import Components.Component;
import Components.MissileContainer;
import Components.Sensor;
import Entities.Coordinates;
import Enum.*;

import java.util.ArrayList;

public class Kochav extends Hermes {
    public Kochav(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Kochav(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }
}
