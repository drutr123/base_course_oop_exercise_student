package AerialVehicles.Drones.Haron;

import Components.Camera;
import Components.Component;
import Components.MissileContainer;
import Components.Sensor;
import Enum.*;
import Entities.Coordinates;

import java.util.ArrayList;

public class Shoval extends Haron {
    public Shoval(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Shoval(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }
}

