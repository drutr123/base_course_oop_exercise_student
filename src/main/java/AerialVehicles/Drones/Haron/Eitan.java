package AerialVehicles.Drones.Haron;

import Components.Component;
import Components.MissileContainer;
import Components.Sensor;
import Entities.Coordinates;
import Enum.*;

import java.util.ArrayList;

public class Eitan extends Haron {
    public Eitan(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Eitan(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }
}
