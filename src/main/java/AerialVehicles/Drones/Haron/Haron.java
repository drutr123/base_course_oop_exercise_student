package AerialVehicles.Drones.Haron;

import AerialVehicles.Drones.Drone;
import Components.Component;
import Entities.Coordinates;
import Enum.FlightStatus;

import java.util.ArrayList;

public abstract class Haron extends Drone {
    private final int MAX_HOURS_FOR_REPAIR = 150;

    public Haron(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public Haron(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }

    @Override
    public int getMaxHoursForRepair() {
        return this.MAX_HOURS_FOR_REPAIR;
    }
}
