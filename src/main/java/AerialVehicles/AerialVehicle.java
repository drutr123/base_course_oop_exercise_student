package AerialVehicles;

import Components.Component;
import Entities.Coordinates;
import Enum.FlightStatus;

import java.util.ArrayList;

public abstract class AerialVehicle {
    private int hoursFromLastRepair;
    private FlightStatus flightStatus;
    private Coordinates baseCoordinates;
    private ArrayList<Component> components;

    public AerialVehicle(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        this.hoursFromLastRepair = hoursFromLastRepair;
        this.flightStatus = flightStatus;
        this.baseCoordinates = baseCoordinates;
        this.components = new ArrayList<>();
    }

    public AerialVehicle(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        this.hoursFromLastRepair = hoursFromLastRepair;
        this.flightStatus = flightStatus;
        this.baseCoordinates = baseCoordinates;
        this.components = components;
    }

    public void addComponent(Component component) {
        this.getComponents().add(component);
    }

    public Component findComponent(Class<?> classRef) {
        for (int i = 0; i < this.getComponents().size(); i++) {
            if (classRef.isInstance(this.getComponents().get(i))) {
                return this.getComponents().get(i);
            }
        }

        return null;
    }

    public boolean hasComponent(Class<?> classRef) {
        return this.findComponent(classRef) != null;
    }

    private ArrayList<Component> getComponents() {
        return components;
    }

    public int getHoursFromLastRepair() {
        return hoursFromLastRepair;
    }

    public void setHoursFromLastRepair(int hoursFromLastRepair) {
        this.hoursFromLastRepair = hoursFromLastRepair;
    }

    public FlightStatus getFlightStatus() {
        return flightStatus;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public Coordinates getBaseCoordinates() {
        return baseCoordinates;
    }

    public void setBaseCoordinates(Coordinates baseCoordinates) {
        this.baseCoordinates = baseCoordinates;
    }

    public void flyTo(Coordinates destination) {
        switch (this.getFlightStatus()) {
            case READY:
                System.out.println("Flying to: " + destination.toString());
                this.setFlightStatus(FlightStatus.IN_AIR);
                break;
            case NOT_READY:
                System.out.println("Aerial Vehicle isn't ready to fly");
                break;
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on: " + destination.toString());
        this.check();
    }

    public void check() {
        if (this.getHoursFromLastRepair() >= this.getMaxHoursForRepair()) {
            this.setFlightStatus(FlightStatus.NOT_READY);
            this.repair();
        } else {
            this.setFlightStatus(FlightStatus.READY);
        }
    }

    public void repair() {
        this.setHoursFromLastRepair(0);
        this.setFlightStatus(FlightStatus.READY);
    }

    public abstract int getMaxHoursForRepair();
}
