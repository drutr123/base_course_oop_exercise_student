package AerialVehicles.FlightPlanes;

import AerialVehicles.AerialVehicle;
import Components.Component;
import Entities.Coordinates;
import Enum.*;

import java.util.ArrayList;

public abstract class FightPlane extends AerialVehicle {
    private final int MAX_HOURS_FOR_REPAIR = 250;

    public FightPlane(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public FightPlane(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }

    @Override
    public int getMaxHoursForRepair() {
        return this.MAX_HOURS_FOR_REPAIR;
    }
}
