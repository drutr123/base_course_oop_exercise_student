package AerialVehicles.FlightPlanes;

import Components.Component;
import Components.MissileContainer;
import Components.Sensor;
import Enum.*;
import Entities.Coordinates;

import java.util.ArrayList;

public class F15 extends FightPlane {
    public F15(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public F15(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }
}
