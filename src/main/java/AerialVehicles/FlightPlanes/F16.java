package AerialVehicles.FlightPlanes;

import Components.Camera;
import Components.Component;
import Components.MissileContainer;
import Enum.*;
import Entities.Coordinates;

import java.util.ArrayList;

public class F16 extends FightPlane {
    public F16(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates);
    }

    public F16(int hoursFromLastRepair, FlightStatus flightStatus, Coordinates baseCoordinates, ArrayList<Component> components) {
        super(hoursFromLastRepair, flightStatus, baseCoordinates, components);
    }
}
